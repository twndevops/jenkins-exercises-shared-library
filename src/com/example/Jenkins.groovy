#!/user/bin/env groovy

// This file should only contain functionality shared across vars in library.

package com.example

// implements Serializable = supports saving state of execution if pipeline is paused then resumed
class Jenkins implements Serializable {
    // to make Groovy pipeline syntax/vars/plugins in Jenkinsfile available in this simple Groovy file
    def script
    Jenkins (script){
        this.script = script
    }
    // methods are available globally
    // script obj makes commands available in Groovy
    def testApp() {
        script.sh "npm i"
        script.echo "Executing tests..."
        script.sh "npm run test"
        // to fail test, change HTML filename and commit change to remote repo
        // by default pipeline should "abort" = skip remaining stages due to test failure
    }
    def incrementVersion() {
        script.echo "Bumping version..."
        // References: https://docs.npmjs.com/updating-your-published-package-version-number, https://docs.npmjs.com/cli/v9/commands/npm-version
        script.sh "npm version patch"
        // following cmd confirms Jenkins' local package.json version was bumped
        // sh "npm pkg get version"
        // Reference: https://www.jenkins.io/doc/pipeline/steps/workflow-basic-steps/#readfile-read-file-from-workspace
        def matches = script.readFile('package.json') =~ '"version": (.+),'
        // echo "Here are the matches: ${matches[0]}" // matches[0]: ["version": "1.0.1",,  "1.0.1"]
        // env vars are available to all pipeline stages
        script.env.IMG_NAME = "${matches[0][1]}-${script.BUILD_NUMBER}"
    }
    def buildImg() {
        script.echo "Building and pushing image..."
        script.sh "docker build -t upnata/module-8-bootcamp-node-project:${script.IMG_NAME} ."
    }
    def dockerLogin() {
        // extract DockerHub creds separately to be used as vars in sh cmd
        script.withCredentials([script.usernamePassword(credentialsId: 'dockerhub-private-repo', usernameVariable: 'USER', passwordVariable: 'PWD')]) {
            script.sh "echo ${script.PWD} | docker login -u ${script.USER} --password-stdin"
        }
    }
    def pushImg() {
        script.sh "docker push upnata/module-8-bootcamp-node-project:${script.IMG_NAME}"
    }
    def gitlabLogin() {
        script.echo "Committing new version..."
        // configure user name and email for Git commit (or Git will throw error)
        script.sh "git config user.name 'jenkins'"
        script.sh "git config user.email 'jenkins@exercises.com'"
        // sh "git config --list && git status && git branch"
        // extract Git creds separately to be used as vars in sh cmds
        script.withCredentials([script.usernamePassword(credentialsId: 'git-creds', usernameVariable: 'USER', passwordVariable: 'PWD')]) {
            // connect Jenkins' locally checked out repo to remote repo and authenticate Jenkins to GL
            script.sh "git remote set-url origin https://${script.USER}:${script.PWD}@gitlab.com/twndevops/jenkins-exercises.git"
        }
    }
    def commitVersion() {
        script.sh "git add package.json"
        script.sh "git commit -m'ci: app patch version bumped'"
        script.sh "git push origin HEAD:${script.BRANCH_NAME}"
    }
}