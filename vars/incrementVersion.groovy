#!/user/bin/env groovy

import com.example.Jenkins

def call() {
    // creating new instance of Jenkins class, passing obj w/ access to Jenkinsfile offerings
    // passing context from this call func to Jenkins class (call = interface)
    return new Jenkins(this).incrementVersion()
}