#!/user/bin/env groovy

import com.example.Jenkins

def call() {
    // creating new instance of Jenkins class, passing obj w/ access to Jenkinsfile offerings
    // passing context from this call func (this) to Jenkins class, so call = interface b/w class and Jenkinsfile func call
    // this global var calls Groovy method defined in Jenkins class in src pkg
    return new Jenkins(this).testApp()
}